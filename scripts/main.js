function main() {
    toggleScale()
    toggleDrag()
    saveScrollPosition()
}

// core
function copyToClipboard(value) {
    navigator.clipboard.writeText(value);
}

function saveScrollPosition() {
    document.addEventListener("DOMContentLoaded", function (event) {
        var scrollpos = localStorage.getItem('scrollpos');
        if (scrollpos) window.scrollTo(0, scrollpos);
    });

    window.onbeforeunload = function (e) {
        localStorage.setItem('scrollpos', window.scrollY);
    };
}

// scale to fit
function toggleScale() {
    scaleToFit()
    addEventListener("resize", (event) => { scaleToFit() });
}

function scaleToFit() {
    scale = document.documentElement.clientWidth / parseInt(document.body.offsetWidth)
    document.documentElement.style.scale = scale
}

// drag images
const canvasNodeList = document.querySelectorAll('content');
const canvasArray = Array.from(canvasNodeList)

let deltaX, deltaY, oldX, oldY, initialX, initialY;
let oldZIndex, parentHeight, parentWidth;

const gridSize = 2.5
const mouseEps = 0.0000001; //epsilon interval
let drag = false;
let toggleDebug = false;

const convert_mm2px = function (mm) {
    var div = document.createElement('div');
    div.style.display = 'block';
    div.style.height = '100mm';
    document.body.appendChild(div);
    var convert = div.offsetHeight * mm / 100;
    div.parentNode.removeChild(div);
    return convert;
};
const roundToNearestGridSize = x => Math.round(x / gridSize) * gridSize
const mm2px = convert_mm2px(1)

function toggleDrag() {
    for (let i = 0; i < canvasNodeList.length; i++) {
        canvas = canvasNodeList[i];
        canvas.addEventListener('mousedown', mouseDownHandler, false);
        canvas.addEventListener('mouseup', mouseUpHandler, false);
    }
}

function mouseDownHandler(e) {
    e.preventDefault();
    if (!canvasArray.includes(e.target)) {
        oldX = initialX = e.clientX;
        oldY = initialY = e.clientY;
        drag = false;

        e.target.addEventListener('mousemove', doDrag, false);
        e.target.addEventListener('mouseup', stopDrag, false);
        e.target.addEventListener('mouseout', stopDrag, false);

        oldZIndex = e.target.style.zIndex
        e.target.style.zIndex = 999
    }
};

function mouseUpHandler(e) {
    e.preventDefault();
    if (Math.abs((initialX - e.clientX)) >= mouseEps && Math.abs((initialY - e.clientY)) >= mouseEps) {
        e.target.addEventListener('click', preventLink, false)
    } else {
        e.target.removeEventListener('click', preventLink, false)
        return
    }
};

function preventLink(e) {
    e.preventDefault();
    return false;
}

function doDrag(e) {
    e = e || window.event;
    e.preventDefault();

    // calculate the new cursor position:
    bodyScale = document.documentElement.style.scale

    deltaX = (oldX - e.clientX) / bodyScale;
    deltaY = (oldY - e.clientY) / bodyScale;
    oldX = e.clientX;
    oldY = e.clientY;

    e.target.style.top = (e.target.offsetTop - deltaY) / mm2px + "mm";
    e.target.style.left = (e.target.offsetLeft - deltaX) / mm2px + "mm";
}

function stopDrag(e) {
    e.target.style.zIndex = oldZIndex

    e.target.removeEventListener('mousemove', doDrag, false);
    e.target.removeEventListener('mouseup', stopDrag, false);
    e.target.removeEventListener('mouseout', stopDrag, false);


    e.target.style.top = roundToNearestGridSize(parseInt(e.target.style.top)) + "mm";
    e.target.style.left = roundToNearestGridSize(parseInt(e.target.style.left)) + "mm";
    if (toggleDebug == true) {
        let canvas = e.target.parentElement
        let v = canvas.innerHTML.trim();
        console.log(v);
        copyToClipboard(v);
    }
}

function design() {
    if (toggleDebug == false) {
        toggleDebug = true;
        for (let i = 0; i < canvasNodeList.length; i++) {
            canvas = canvasNodeList[i];
            canvas.style.border = "1px solid red"
        }
    } else if (toggleDebug == true) {
        toggleDebug = false;
        for (let i = 0; i < canvasNodeList.length; i++) {
            canvas = canvasNodeList[i];
            canvas.style.border = ""
        }
    }
}

main()